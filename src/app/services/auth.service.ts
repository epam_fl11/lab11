import {Injectable, NgZone} from '@angular/core';
import {BehaviorSubject, from, Observable} from 'rxjs';
import {AngularFireAuth} from '@angular/fire/auth';
import {User} from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements IAuth {
  protected currentUser: BehaviorSubject<any>;

  constructor(private fireAuth: AngularFireAuth, private zone: NgZone) {
    this.currentUser = new BehaviorSubject<any>(null);

    fireAuth.auth.onAuthStateChanged(user => {
       this.zone.run(() => this.currentUser.next(user));
    });
  }

  public get user(): Observable<any> {
    return this.currentUser;
  }

  public login(email: string, password: string): Observable<any> {
    return from(this.fireAuth.auth.signInWithEmailAndPassword(email, password));
  }

  public create(email: string, password: string): Observable<any> {
    return from(this.fireAuth.auth.createUserWithEmailAndPassword(email, password));
  }

  public resetPassword(email: string): Observable<any> {
    return from(this.fireAuth.auth.sendPasswordResetEmail(email));
  }
}

interface IAuth {
  readonly user: Observable<any>;

  login(email: string, password: string): Observable<any>;

  create(email: string, password: string): Observable<any>;

  resetPassword(email: string): Observable<any>;
}
