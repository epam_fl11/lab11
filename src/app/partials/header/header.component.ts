import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass'],
})
export class HeaderComponent implements OnInit {
  public isAuth: Observable<boolean>;

  constructor(protected auth: AuthService) { }

  ngOnInit() {
    this.isAuth = this.auth.user
      .pipe(
        map((value: any) => !!value)
      );
  }
}
