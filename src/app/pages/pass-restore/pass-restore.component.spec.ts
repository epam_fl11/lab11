import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PassRestoreComponent } from './pass-restore.component';

describe('PassRestoreComponent', () => {
  let component: PassRestoreComponent;
  let fixture: ComponentFixture<PassRestoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PassRestoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PassRestoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
