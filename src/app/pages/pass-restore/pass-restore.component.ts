import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-pass-restore',
  templateUrl: './pass-restore.component.html',
  styleUrls: ['./pass-restore.component.sass']
})
export class PassRestoreComponent implements OnInit {
  public isSent: boolean;
  public form: FormGroup;
  public usersEmail: string;

  constructor(protected fb: FormBuilder,
              protected auth: AuthService) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]]
    });
  }

  public get email(): AbstractControl {
    return this.form.get('email');
  }

  public resetPassword() {
    if (this.form.invalid) {
      return;
    }

    this.usersEmail = this.email.value;

    this.auth.resetPassword(this.usersEmail)
    .subscribe(() => {
      this.isSent = true;
      this.form.reset();
    }, (error: any) => {
      const { code: errorCode } = error;

      switch (errorCode) {
        case 'auth/invalid-email':
          this.email.setErrors({ email: true });
          break;
        case 'auth/user-disabled':
          this.email.setErrors({ disabled: true });
          break;
        case 'auth/user-not-found':
          this.email.setErrors({ notFound: true });
          break;
      }
    });
  }

  public getErrorKey(errors) {
    return Object.keys(errors)[0];
  }
}
