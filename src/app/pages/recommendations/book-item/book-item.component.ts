import { Component, OnInit, Input } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-book-item',
  templateUrl: './book-item.component.html',
  styleUrls: ['./book-item.component.sass']
})
export class BookItemComponent implements OnInit {

  @Input()
  public title: string;

  @Input()
  public author: string;

  @Input()
  public imageSrc: string;

  public imageSafeSrc: SafeResourceUrl;

  constructor(public sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.imageSafeSrc = this.sanitizer.bypassSecurityTrustResourceUrl(this.imageSrc);
  }

}
