import { Component, OnInit, Input } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-video-item',
  templateUrl: './video-item.component.html',
  styleUrls: ['./video-item.component.sass']
})
export class VideoItemComponent implements OnInit {

  @Input()
  public title: string;

  /**
   * Video resource link.
   */
  @Input()
  public videoUrl: string;

  public videoUrlSafe: SafeResourceUrl;

  constructor(public sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.videoUrlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(this.videoUrl);
  }

}
