import { Component, OnInit, Input } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-site-item',
  templateUrl: './site-item.component.html',
  styleUrls: ['./site-item.component.sass']
})
export class SiteItemComponent implements OnInit {

  @Input()
  public link: string;

  public linkSafe: SafeResourceUrl;

  constructor(public sanitizer: DomSanitizer) {  }

  ngOnInit() {
    this.linkSafe = this.sanitizer.bypassSecurityTrustUrl(this.link);
  }

}
