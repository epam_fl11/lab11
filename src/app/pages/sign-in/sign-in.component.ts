import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.sass']
})
export class SignInComponent implements OnInit {
  public profileForm: FormGroup;
  public hide = true;

  constructor(private auth: AuthService, protected fb: FormBuilder) {
  }

  public get email(): AbstractControl {
    return this.profileForm.get('email');
  }

  public get password(): AbstractControl {
    return this.profileForm.get('password');
  }

  public login() {
    if (this.profileForm.invalid) {
      return;
    }

    const { email, password } = this.profileForm.value;

    this.auth.login(email, password)
    .subscribe(() => {
      alert('You\'ve successfully entered into account.');
    }, (error: any) => {
      const { code: errorCode } = error;

      switch (errorCode) {
        case 'auth/invalid-email':
          this.email.setErrors({ invalid: true });
          break;
        case 'auth/user-disabled':
          this.email.setErrors({ disabled: true });
          break;
        case 'auth/user-not-found':
          this.email.setErrors({ notFound: true });
          break;
        case 'auth/wrong-password':
          this.password.setErrors({ wrong: true });
          break;
      }
    });
  }

  public getError(errors) {
    if (!errors) {
      return '';
    }
    return Object.keys(errors)[0];
  }

  ngOnInit() {
    this.profileForm = this.fb.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      password: ['', [
        Validators.required,
        Validators.minLength(8)
      ]]
    });
  }
}
