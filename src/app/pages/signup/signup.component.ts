import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  Validators
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { AuthService } from 'src/app/services/auth.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.touched);
    const invalidParent = !!(control && control.touched && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.sass']
})
export class SignupComponent implements OnInit {
  public signupForm: FormGroup;
  public matcher = new MyErrorStateMatcher();
  public hide = true;
  public minPassLength = 8;
  public isSuccess = false;
  public errorMessage = '';

  public constructor(private fb: FormBuilder, protected auth: AuthService) {
  }

  ngOnInit() {
    this.signupForm = this.fb.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      passwordGroup: this.fb.group({
        password: ['', [
          Validators.required,
          Validators.minLength(this.minPassLength)
        ]],
        confirmPassword: ['', [
          Validators.required
        ]]
      }, { validators: this.checkPasswords })
    });
  }

  public get email() {
    return this.signupForm.get('email');
  }

  public get password() {
    return this.signupForm.get('passwordGroup.password');
  }

  public get confirmPassword() {
    return this.signupForm.get('passwordGroup.confirmPassword');
  }

  public onSubmit() {
    if (this.signupForm.invalid) {
      return;
    }

    const { email, passwordGroup: { password } } = this.signupForm.value;

    this.auth.create(email, password)
    .subscribe(() => {
      this.isSuccess = true;
      alert(`Your email ${email} was registered and you can use all services now as Authorized User.`);
    }, (error: any) => {
      const { code: errorCode, message } = error;
      this.errorMessage = message;

      switch (errorCode) {
        case 'auth/email-already-in-use':
          this.email.setErrors({
            email_exist: true
          });
          break;
        case 'auth/invalid-email':
          this.email.setErrors({
            invalid_email: true
          });
          break;
        case 'auth/operation-not-allowed':
          this.email.setErrors({
            operation_not_allowed: true
          });
          break;
      }
    });
  }

  public getErrorCode(...errors: any[]) {
    if (!errors) {
      return '';
    }

    return Object.keys(Object.assign({}, ...errors))[0];
  }

  public checkPasswords(group: FormGroup) {
    const { password, confirmPassword } = group.value;

    return password === confirmPassword ? null : { notSame: true };
  }
}
