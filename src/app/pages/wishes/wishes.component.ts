import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-wishes',
  templateUrl: './wishes.component.html',
  styleUrls: ['./wishes.component.sass']
})
export class WishesComponent implements OnInit {
  public isSent = false;
  public form: FormGroup;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.minLength(5)]],
      message: ['', [Validators.required, Validators.minLength(5)]],
    });
  }

  public sendMessage() {
    console.log(this.form.value);

    this.isSent = true;
    this.form.reset();
  }

  public sendMoreMessage() {
    this.isSent = false;
  }
}
