export class User {
  public id: number;
  public email: string;
  public password: string;
  public firstName: string;
  public lastName: string;
  public isAdmin: boolean;

  public constructor(data = {}) {
    Object.assign(this, data);
  }
}
