import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NotFoundComponent} from './pages/page-not-found/page-not-found.component';
import { HomeComponent } from './pages/home/home.component';
import { IdeaComponent } from './pages/idea/idea.component';
import { RecommendationsComponent } from './pages/recommendations/recommendations.component';
import { HeaderComponent } from './partials/header/header.component';
import { FooterComponent } from './partials/footer/footer.component';
import { WishesComponent } from './pages/wishes/wishes.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { PassRestoreComponent } from './pages/pass-restore/pass-restore.component';
import { SignupComponent } from './pages/signup/signup.component';
import { MatFormFieldModule,  } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule, MatCardModule } from '@angular/material';
import { SignInComponent } from './pages/sign-in/sign-in.component';
import { BookItemComponent } from './pages/recommendations/book-item/book-item.component';
import { SiteItemComponent } from './pages/recommendations/site-item/site-item.component';
import { VideoItemComponent } from './pages/recommendations/video-item/video-item.component';
import { IdeaItemComponent } from './pages/idea/idea-item/idea-item.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';

/**
 * About firebase collection
 * @see https://angular-templates.io/tutorials/about/angular-crud-with-firebase
 * @see https://github.com/angular/angularfire2/issues/954
 *
 * About firebase signup/signin
 * @see https://www.techiediaries.com/angular-course-firebase-authentication/
 *
 */


@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    HomeComponent,
    IdeaComponent,
    RecommendationsComponent,
    HeaderComponent,
    FooterComponent,
    WishesComponent,
    PassRestoreComponent,
    SignupComponent,
    SignInComponent,
    BookItemComponent,
    SiteItemComponent,
    VideoItemComponent,
    IdeaItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
