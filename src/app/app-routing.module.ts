import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { HomeComponent } from './pages/home/home.component';
import { IdeaComponent } from './pages/idea/idea.component';
import { RecommendationsComponent } from './pages/recommendations/recommendations.component';
import { WishesComponent } from './pages/wishes/wishes.component';
import { PassRestoreComponent } from './pages/pass-restore/pass-restore.component';
import { SignupComponent } from './pages/signup/signup.component';
import { SignInComponent } from './pages/sign-in/sign-in.component';
import { AuthGuard } from './guards/auth.guard';
import { NotAuthGuard } from './guards/not-auth.guard';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/home' },
  { path: 'home', component: HomeComponent },
  {
    path: 'profile',
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    loadChildren: () => import('./profile/profile.module')
      .then(module => module.ProfileModule)
  },
  { path: 'idea', component: IdeaComponent },
  { path: 'wishes', component: WishesComponent },
  { path: 'recommendations', component: RecommendationsComponent },
  { path: 'restore', component: PassRestoreComponent, canActivate: [NotAuthGuard] },
  { path: 'signup', component: SignupComponent, canActivate: [NotAuthGuard] },
  { path: 'signin', component: SignInComponent, canActivate: [NotAuthGuard] },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
