export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyC1qCxC5PbKoTP8d0PcJm3EQTsrx_GvG2s',
    authDomain: 'epam-lb11-grape.firebaseapp.com',
    databaseURL: 'https://epam-lb11-grape.firebaseio.com',
    projectId: 'epam-lb11-grape',
    storageBucket: 'epam-lb11-grape.appspot.com',
    messagingSenderId: '159355956226'
  }
};
